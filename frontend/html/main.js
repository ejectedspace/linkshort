var inp = document.getElementById('url')
inp.addEventListener("keyup", function(event) {
    if (event.keyCode === 13) {
      short()
    }
});

function short(){
    document.getElementById('error').innerHTML = ''
    let url = document.getElementById('url').value
    if(ValidURL(url)){
        let http = new XMLHttpRequest()
        http.open('GET', '/api/?uri=' + url, false)
        http.onreadystatechange=(e)=>{
            let res = JSON.parse(http.response)
            document.getElementById('out').innerHTML =  '<a href="https://k.1ll.me/i/' + res.shortUrl + '">https://k.1ll.me/i/' + res.shortUrl + '</a>'
        }
        http.send()
    }else{
        document.getElementById('error').innerHTML = '<div class="error">not a url</div>'
    }
}

function ValidURL(url) {
    var regexp = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/
    return regexp.test(url);
 }  