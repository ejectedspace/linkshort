{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
module Main where

import Web.Scotty
import Data.Monoid ((<>))
import qualified System.Random as SR
import Control.Monad (replicateM)
import qualified Database.Redis as R
import Control.Monad.Trans
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BL
import qualified Data.ByteString.Char8 as BC
import Data.Text.Encoding (decodeUtf8, encodeUtf8)
import qualified Data.Text.Lazy as TL
import Data.Aeson (FromJSON, ToJSON)
import GHC.Generics
import System.Environment
import Data.Maybe

data Url = Url { url :: TL.Text, shortUrl :: TL.Text} deriving (Show, Generic)
instance ToJSON Url
instance FromJSON Url

alphaNum = ['a' .. 'z'] ++ ['A'..'Z'] ++ ['0'..'9']
randomElement l = SR.randomRIO (0, ((length l) - 1)) >>= \d -> return (l !! d)

shortyGen = replicateM 6 (randomElement alphaNum)


main :: IO ()
main = do
  -- get db url form env var if it exists else default to "db"
  dbUrlEnv <- lookupEnv "LINKSHORT_DB_URL"
  let dbUrl = fromMaybe "db" dbUrlEnv
      confInfo = R.defaultConnectInfo { R.connectHost =  dbUrl }

  putStrLn "Starting Server..."
  conn <- R.connect confInfo
  scotty 3000 $ do
    get "/" $ do
      longUrl  <- param "uri";
      _temp <- param "uri"
      shortUrl <- liftIO shortyGen
      let uri = TL.pack shortUrl
      let _uri = BC.pack shortUrl
      res <- liftIO (R.runRedis conn $ R.set _uri _temp)
      json (Url { url = longUrl, shortUrl = uri })
    get "/:short" $ do
      short <- param "short"
      _temp <- param "short"
      let _short = TL.pack _temp
      uri <- liftIO (R.runRedis conn $ R.get short);
      case uri of
        Left reply -> text (TL.pack (show reply))
        Right mbBS -> case mbBS of
          Nothing -> raise "Not Found" `rescue` (\msg -> text msg)
          Just bs -> redirect uri
            where uri = TL.fromStrict (decodeUtf8 bs)
