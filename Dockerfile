FROM ejectedspace/stack-musl as builder
# COMPILE PID1
ADD . /usr/src/linkshort
WORKDIR /usr/src/linkshort
RUN hpack
RUN cabal new-update
RUN cabal install --ghc-options='-optl-static -fPIC -optc-Os'
#RUN stack --local-bin-path ./dist install --ghc-options='-optl-static -fPIC -optc-Os'
# SHOW INFORMATION ABOUT PID1
RUN ldd /root/.cabal/bin/linkShort || true
RUN du -hs /root/.cabal/bin/linkShort 

FROM scratch
COPY --from=builder /root/.cabal/bin/linkShort /linkShort
CMD ["/linkShort"]
